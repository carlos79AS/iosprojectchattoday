//
//  AnadirContactoViewController.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/7/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseFirestore

class AnadirContactoViewController: UIViewController {
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var emailTextFieldAC: UITextField!
    var arrayUsuarios = [String]()
    let firestore = Firestore.firestore()
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfUsserIsLogged()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func savePressed(_ sender: Any) {
        var email: String
        email = emailTextFieldAC.text!
        var usuarioExiste = false
        //comprobar si el usuario exist een la base
        for usuario in arrayUsuarios{
            if usuario == email{
                usuarioExiste = true
                break
            }else{
                usuarioExiste = false
            }
            
        }
        if !usuarioExiste{
            var alert = UIAlertController(title: "Hey", message: "This user doesnt exist", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            //TODO crear una coleccion con el nuevo contacto
            let id = (Auth.auth().currentUser?.uid)!
            
            //var firestoreLectura = firestore.collection("DatosUsuarioAdmin-PC").document(id).documentID
            //print(firestoreLectura)
            var db = firestore.collection("DatosUsuarioAdmin-PC").document(id).collection("contactos").document()
            db.setData(
                [
                    "email": email ?? "",
                    
            ]) { (error) in
                
                if error != nil {
                    self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
                
                if error == nil {
                    print(error)
                }
            }
        }
    }
    
    func checkIfUsserIsLogged(){
        
        if Auth.auth().currentUser?.uid == nil {
            handleLogout()
        }else{
            let id = Auth.auth().currentUser?.uid
            firestore
                .collection("DatosUsuarioAdmin-PC")
                .getDocuments { (snapshot, error) in
                    
                    if error != nil {
                        print(error)
                        return
                    }
                    for document in snapshot!.documents {
                        self.showUser(id: document.documentID)
                    }
            }
        }
    }
    
    func showUser(id: String) {
        
        if id == String(Auth.auth().currentUser!.uid) {
            return
        }
        
        firestore
            .collection("DatosUsuarioAdmin-PC")
            .document(id)
            .addSnapshotListener { (snapshot, error) in
                if error != nil {
                    print(error)
                    return
                }
                let data = snapshot?.get("email")
                print(data)
                var dataString: String
                dataString = data as! String
                self.arrayUsuarios.append(dataString)
        }
    }
    
    func handleLogout(){
        
        self.performSegue(withIdentifier: "registerSucessSegue", sender: self)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
}
