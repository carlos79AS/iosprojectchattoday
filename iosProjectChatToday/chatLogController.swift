//
//  chatLogController.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseFirestore

class chatLogController : UIViewController {
    let id = Auth.auth().currentUser?.uid
    //@IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var inputText: UITextField!    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    var userDest = "f@email.com"
    @IBAction func sendButtonPressed(_ sender: Any) {
        saveToFireStoreChat(userUID: id!, userDest: userDest, mensaje: inputText.text!)
        print(inputText.text ?? "quert")
    }
    func saveToFireStoreChat(userUID: String, userDest: String, mensaje: String) {
        let firestore = Firestore.firestore()
        
        let _ = firestore.collection("mensajes").document(userUID).setData(
            [
                "fuente": userUID ?? "",
                "destino": userDest ?? "",
                "mensaje": mensaje ?? ""
        ])
    }
}
