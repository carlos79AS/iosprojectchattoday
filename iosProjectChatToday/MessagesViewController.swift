//
//  MessagesViewController.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import Firebase
import FirebaseUI


struct  NellPAstel  {
    
    
    let messageO: String
    
}
struct Message {
    var username : String
    
    
    
}
class MessagesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        createContactArray()

        // Do any additional setup after loading the view.
    }
    var messages : [Message] = [Message]()
    
    //let datas: [NellPAstel] = [NellPAstel(destinatare: "", message: "")]
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        createContactArray()
//
//    }
    func createContactArray() {
        let db = Firestore.firestore()
        db.collection("mensajes").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var listTemp : [Message] = [Message]()
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    
                    listTemp.append(Message(username: document.data()["mensaje"] as! String))
                }
                self.messages = listTemp
            }
            //self.tableView.reloadData()
            
        }
    }

    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return messages.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        createContactArray()
        let storage = Storage.storage()
        
        //let storageRef = storage.reference()
        //let imageRef = storageRef.child("DatosUsuarioAdmin-PC")
        
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell",for: indexPath ) as! MessagesViewCell
        let texts = messages[indexPath.row]
        cell.messageTwo?.text = texts.username
        //cell.messageLabel?.text = texts.contactImg
        
        // Reference to an image file in Firebase Storage
        
        //let reference = storageRef.child("users-Admin/"+texts.contactsID+".jpg")
        // UIImageView in your ViewController
        //let imageView: UIImageView = cell.imageCell
        // Placeholder image
        //  let placeholderImage = UIImage(named: headline.profesorDocID+"placeholder.jpg")
        // Load the image using SDWebImage
        
        //cell.imageCell.sd_setImage(with: reference, placeholderImage: UIImage(named: texts.contactsID+"placeholder.jpg"))
        
        return cell
    }
    let id = Auth.auth().currentUser?.uid
@IBOutlet weak var inputText: UITextField!
   
    var userDest = "f@email.com"
    @IBAction func saveButton(_ sender: Any) {
        saveToFireStoreChat(userUID: id!, userDest: userDest, mensaje: inputText.text!)
        print(inputText.text ?? "quert")
    }
    func saveToFireStoreChat(userUID: String, userDest: String, mensaje: String) {
            let firestore = Firestore.firestore()
            
            let _ = firestore.collection("mensajes").document(userUID).setData(
                [
                    "fuente": userUID ?? "",
                    "destino": userDest ?? "",
                    "mensaje": mensaje ?? ""
            ])
        }
  

}
