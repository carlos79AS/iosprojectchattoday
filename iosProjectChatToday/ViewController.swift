//
//  ViewController.swift
//  iosProjectChatToday
//
//  Created by Carlos on 1/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseStorage

class ViewController: UIViewController {
    @IBOutlet weak var registerButton: UIButton!
    
    
    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func whenButtonIsClicked(action: @escaping () -> Void) {
        self.performSegue(withIdentifier: "registerSegue", sender: registerButton)
        
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        guard let email = emailText.text,
            let password = passwordText.text else {
                return
                
        }
        
        
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            // asincrono, se ejecuta en cualesqueir tiempo.
            print("Result: \(result)")
            print("Error: \(error)")
            if error != nil {
                self.presentAlertWith(title: "ERROR", message: error?.localizedDescription ?? "Ups! Salado Compa")
            } else {
                self.performSegue(withIdentifier: "loginSegue", sender: self)
                
            }
            
        }
    }
    
    private func presentAlertWith(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "GO", style: .default) { (_) in
            
            self.emailText.text = ""
            self.passwordText.text = ""
            self.emailText.becomeFirstResponder()
        }
        alertController.addAction(okAlertAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func comprobarUsuarioLogeado(){
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        }else{
            let uid = Auth.auth().currentUser?.uid
          
            let storage = Storage.storage()
                  
                  let ref = storage.reference()
        }
        
    }
    
    @objc func handleLogout(){
        
        
    }
    
}
