//
//  TableViewController.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import Firebase
import FirebaseUI


struct  Messsage  {
    
    let destinatare: String
    let message: String
    
}

struct Contact {
    var username : String
    
    var contactImg : String
    var contactsID: String
}

class TableViewController: UITableViewController {
    var contacts : [Contact] = [Contact]()
    
    let datas: [Messsage] = [Messsage(destinatare: "", message: "")]
    
    @IBOutlet weak var textNENE: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        createContactArray()
        
    }
    func createContactArray() {
        let db = Firestore.firestore()
        db.collection("DatosUsuarioAdmin-PC").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var listTemp : [Contact] = [Contact]()
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    
                    listTemp.append(Contact(username: document.data()["name"] as! String, contactImg: document.data()["imageURL"] as! String,contactsID: document.documentID))
                }
                self.contacts = listTemp
            }
            self.tableView.reloadData()
        }
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        createContactArray()
        let storage = Storage.storage()
        
        let storageRef = storage.reference()
        //let imageRef = storageRef.child("DatosUsuarioAdmin-PC")
        
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell",for: indexPath ) as! ChatsListTableViewCell
        let texts = contacts[indexPath.row]
        cell.nameLabel?.text = texts.username
        cell.messageLabel?.text = texts.contactImg
        
        // Reference to an image file in Firebase Storage
        
        let reference = storageRef.child("users-Admin/"+texts.contactsID+".jpg")
        // UIImageView in your ViewController
        let imageView: UIImageView = cell.imageCell
        // Placeholder image
        //  let placeholderImage = UIImage(named: headline.profesorDocID+"placeholder.jpg")
        // Load the image using SDWebImage
        
        cell.imageCell.sd_setImage(with: reference, placeholderImage: UIImage(named: texts.contactsID+"placeholder.jpg"))
        
        return cell
    }
    
    @IBAction func addButton(_ sender: Any) {
        let alertCtrl = UIAlertController(title: "Add Message", message: "What would you like to say?", preferredStyle: .alert)
        alertCtrl.addTextField(configurationHandler: textNENE )
        


        present(alertCtrl, animated: true)
    }
}
