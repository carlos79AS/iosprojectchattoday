//
//  MessagesViewCell.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit

class MessagesViewCell: UITableViewCell {

    @IBOutlet weak var messageOne: UILabel!
    
    @IBOutlet weak var messageTwo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
