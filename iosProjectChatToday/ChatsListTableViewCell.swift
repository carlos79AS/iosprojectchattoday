//
//  ChatsListTableViewCell.swift
//  iosProjectChatToday
//
//  Created by Carlos on 2/13/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit

class ChatsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    //@IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
        // @IBOutlet weak var messageLabel: UILabel!
         
    //@IBOutlet weak var imageCell: UIImageView!
    
         override func awakeFromNib() {
             super.awakeFromNib()
             // Initialization code
         }
     
         override func setSelected(_ selected: Bool, animated: Bool) {
             super.setSelected(selected, animated: animated)
     
             // Configure the view for the selected state
         }

}
