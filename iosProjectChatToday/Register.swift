//
//  Register.swift
//  iosProjectChatToday
//
//  Created by Carlos on 1/14/20.
//  Copyright © 2020 Carlos. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseFirestore

class Register: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    
    //@IBOutlet weak var pickerUI: UIDatePicker!
    @IBOutlet weak var birthtext: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var nicktext: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var pictureView: UIImageView!
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    let datePicker = UIDatePicker()
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameText.becomeFirstResponder()
        birthtext.delegate = self
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateViewPicker(_:)), for: .valueChanged)
        
        activityView.hidesWhenStopped = true
        activityView.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func dateViewPicker(_ sender: UIDatePicker) {
        let format = DateFormatter()
        format.dateFormat = "dd-MMM-YYYY"
        birthtext.text = format.string(from: datePicker.date )
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        activityView.isHidden = false
        activityView.startAnimating()
        
        createUser{ (userUID) in
            self.uploadImage(userUID: userUID) { (imageURL) in
                self.saveToFireStore(userUID: userUID, imageURL: imageURL){
                    self.activityView.stopAnimating()
                    self.performSegue(withIdentifier: "registerSuccessSegue2", sender: self)
                }
            }
        }
    }
    @IBAction func cameraButton(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Add Picture", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            (_) in
        }
        let libraryAction = UIAlertAction(title: "Library", style: .default){
            (_) in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            
        }
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        present(actionSheet, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pictureView.image = info    [.editedImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == birthtext.tag {
            textField.inputView = datePicker
        }
    }
    
    func uploadImage(userUID: String, completionHandler: @escaping (String)->()){
        let storage = Storage.storage()
        let ref = storage.reference()
        let profileImage = ref.child("users-Admin")
        
        let userPicture = profileImage.child("\(userUID).jpg")
        
        let data = pictureView.image?.jpegData(compressionQuality: 0.5)!
        
        
        let _ = userPicture.putData(data!, metadata: nil) { (metadata, error) in
            
            if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            
            guard let _ = metadata else {
                return
            }
            userPicture.downloadURL {(url, error) in
                if error != nil {
                    self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
                guard let downloadURL = url else {
                    return
                }
                completionHandler(downloadURL.absoluteString)
            }
        }
    }
    
    func createUser (completionHandler: @escaping (String)->()) {
        let auth = Auth.auth()
        auth.createUser(withEmail: emailText.text!, password: passwordText.text!) { (authInfo, error) in
            
            if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            if error == nil {
                completionHandler(authInfo!.user.uid)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func saveToFireStore(userUID: String, imageURL: String, completationHandler: @escaping ()->() ) {
        let firestore = Firestore.firestore()
        
        let _ = firestore.collection("DatosUsuarioAdmin-PC").document(userUID).setData(
            [
                "name": self.nameText.text ?? "",
                "nickname": self.nicktext.text ?? "",
                "email": self.emailText.text ?? "",
                "birthDate":self.birthtext.text ?? "",
                "imageURL": imageURL
                
        ]) { (error) in
            
            if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            if error == nil {
                completationHandler()
            }
        }
    }
}

